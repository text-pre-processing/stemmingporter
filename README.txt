_______________________________________________________________________________
                    Porter Stemming Algorithm in Python
    Author of algorithm: Martin F. Porter
    Author of this implementation: Vivake Gupta <v@nano.com>
    Adaptation: Jonathan Rojas-Sim�n <IDS_Jonathan_Rojas@hotmail.com>
_______________________________________________________________________________
Este componente realiza la reducci�n de palabras a su forma ra�z de acuerdo al
algoritmo de Porter (Porter, 1980). La versi�n can�nica de este algoritmo est� 
escrita en ANSI C por su autor, sin embargo, esta versi�n contiene todas las 
especificaciones de la original.

En esta implementaci�n se realizaron algunos cambios a la versi�n de Vivake 
Gupta, con la finalidad de hacer el funcionamiento de este algoritmo con un 
mayor n�mero y tipo archivos posible. Adem�s, se realizaron algunos cambios
con respecto al uso de par�metros, para hacer m�s entendible su ejecuci�n por
el usuario.
_______________________________________________________________________________

NOTA: Este componente fue probado con Python 3.5
_______________________________________________________________________________

SINTAXIS

python Stemmer-Porter.py (-ID:str | --IDIR:str) (-IE:str | --IEXT:str) 
						 (-OD:str | --ODIR:str)

Donde:

-ID | --IDIR    Es el nombre del directorio que contiene los archivos a 
                procesar.
-IE | --IEXT    Estensi�n de los archivos contenidos en -ID � --IDIR. 
                La extensi�n a ingresar debe contener un "." en su inicio 
                (Ej. .txt, .sum, .pyr, .xxx)
-OD | --ODIR    Directorio donde se almacenar�n los archivos procesados por
                el stemming de Porter.

Ejemplo

python Stemmer-Porter.py -ID IN -IE .txt -OD OUT

La l�nea anterior especificar�a los par�metros debajo:

-ID 	IN 
-IE 	.txt
-OD 	OUT

El programa listar� el contenido del directorio IN a partir de la extensi�n 
.txt determinada por el par�metro -IE. Posteriormente se realiza la lectura de
cada archivo contenido en IN para ingresarlo al stemming de Porter, el cual 
arrojar� una salida de cada archivo y ser� escrita con el mismo nombre del 
archivo le�do, sin embargo, su almacenamiento quedar� en el directorio OUT, 
especificado por el par�metro -OD
_______________________________________________________________________________

REFERENCE

(Porter, 1980)  Porter, 1980, An algorithm for suffix stripping, Program
                Vol. 14, no. 3, pp 130-137,
_______________________________________________________________________________
                            ORIGINAL DESCRIPTION
_______________________________________________________________________________
Source code: https://tartarus.org/martin/PorterStemmer/python.txt
Adaptation: Jonathan Rojas-Sim�n <IDS_Jonathan_Rojas@hotmail.com>
_______________________________________________________________________________

Porter Stemming Algorithm
This is the Porter stemming algorithm, ported to Python from the
version coded up in ANSI C by the author. It may be be regarded
as canonical, in that it follows the algorithm presented in

Porter, 1980, An algorithm for suffix stripping, Program, Vol. 14,
no. 3, pp 130-137,

only differing from it at the points maked --DEPARTURE-- below.

See also http://www.tartarus.org/~martin/PorterStemmer

The algorithm as described in the paper could be exactly replicated
by adjusting the points of DEPARTURE, but this is barely necessary,
because (a) the points of DEPARTURE are definitely improvements, and
(b) no encoding of the Porter stemmer I have seen is anything like
as exact as this version, even with the points of DEPARTURE!

Vivake Gupta (v@nano.com)

Release 1: January 2001

Further adjustments by Santiago Bruno (bananabruno@gmail.com)
to allow word input not restricted to one word per line, leading
to:

release 2: July 2008
