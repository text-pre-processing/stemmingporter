_______________________________________________________________________________
		     		Porter Stemming Algorithm in Python
	Author of algorithm: Martin F. Porter
	Author of this implementation: Vivake Gupta <v@nano.com>
	Adaptation: Jonathan Rojas-Sim�n <IDS_Jonathan_Rojas@hotmail.com>
_______________________________________________________________________________
Este archivo README realiza el stemming de Porter a partir de los documentos
originales de DUC-2002 (DUC, 2002) y los almacena en un directorio dado por el
usuario.
_______________________________________________________________________________
EJECUCI�N

python Stemmer-Porter.py -ID IN -IE .txt -OD OUT

La l�nea anterior especificar�a los par�metros debajo:

-ID 	IN 
-IE 	.txt
-OD 	OUT

El programa listar� el contenido del directorio IN a partir de la extensi�n 
.txt determinada por el par�metro -IE. Posteriormente se realiza la lectura de
cada archivo contenido en IN para ingresarlo al stemming de Porter, el cual 
arrojar� una salida de cada archivo y ser� escrita con el mismo nombre del 
archivo le�do, sin embargo, su almacenamiento quedar� en el directorio OUT, 
especificado por el par�metro -OD
_______________________________________________________________________________

REFERENCES

(Porter, 1980)  Porter, 1980, An algorithm for suffix stripping, Program
				Vol. 14, no. 3, pp 130-137,
(DUC, 2002) 	Document Understanding Conference. (2002). Recuperado de: 
				http://www-nlpir.nist.gov/projects/duc/
